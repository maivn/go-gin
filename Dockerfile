FROM golang
ENV GO111MODULE=off

RUN apt-get update
RUN apt-get install -y libreoffice-writer

RUN go get "github.com/unidoc/unioffice"
RUN go build -i github.com/unidoc/unioffice/...
RUN go get "golang.org/x/net/publicsuffix"
RUN go get "github.com/go-redis/redis"
RUN go get "github.com/lib/pq"
RUN go get "gitlab.com/maivn/rutime"
RUN go get "github.com/jinzhu/gorm"
RUN go get "github.com/gin-gonic/gin"
RUN go get "github.com/json-iterator/go"
RUN go get "github.com/joho/godotenv"
RUN go get "github.com/satori/go.uuid"
RUN go get "gopkg.in/go-playground/validator.v9"
RUN go get "github.com/dgrijalva/jwt-go"
RUN go get "go.uber.org/dig"
RUN go get "github.com/spf13/viper"
RUN go get "github.com/subosito/gotenv"

EXPOSE 8080
